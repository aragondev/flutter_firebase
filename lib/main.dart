import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase/data/auth_dao.dart';
import 'package:flutter_firebase/screen/home_screen.dart';
import 'package:flutter_firebase/screen/login_screen.dart';
import 'package:flutter_firebase/screen/register_screen.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

 @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Firebase Flutter',
      theme: ThemeData(
        primaryColor: const Color(0xFF3D814A),
      ),
      initialRoute: 'login',
      routes: {
        'login':(BuildContext context){
          if(AuthDao().isLoggedIn()){
            return HomeScreen();
          }else{
            return LoginScreen();
          }
        },
        'register':(BuildContext context)=>RegisterScreen(),
        'home':(BuildContext context)=>HomeScreen()
      },
    );
  }
}