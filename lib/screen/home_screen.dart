import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_firebase/data/auth_dao.dart';
import 'package:flutter_firebase/data/user.dart';
import 'package:flutter_firebase/data/user_dao.dart';
import 'package:flutter_firebase/screen/login_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late UserRepository _userRepository;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((timeStamp) {
      _userRepository = UserRepository();
    });
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 100,
            width: double.infinity,
            decoration: BoxDecoration(color: Colors.indigo),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Card(
              child: ListTile(
                title: Text('dasds'),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          AuthDao().logout();
          Navigator.popAndPushNamed(context, 'login');
        },
        child: Icon(Icons.logout),
      ),
    );
  }
}
