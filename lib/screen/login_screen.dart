import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_firebase/data/auth_dao.dart';
import 'package:flutter_firebase/data/user_dao.dart';
import 'package:flutter_firebase/screen/register_screen.dart';
import 'package:flutter_firebase/widgets/appbar_widget.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final _userR = UserRepository();
  final _auth = AuthDao();
  bool validateEmail = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            _appBarLogin(),            
            _formLogin(context),
            _footerLogin()
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Widget _formLogin(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:20,right: 30,left: 30),
      child: Form(
          key: _formKey,
          child: Column(
            children: [_emailInput(), _passwordInput(), _buttonLogin()],
          )),
    );
  }

  Widget _appBarLogin() {
    return AppBarWidget(
              title: 'Login',
              url: 'login.png',
            );
  }

  Widget _emailInput() {
    return TextFormField(
      controller: _emailController,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Email Required';
        }
      },
      decoration: InputDecoration(
          label: Text('Email'), prefixIcon: Icon(LineIcons.mailBulk)),
    );
  }

  Widget _passwordInput() {
    return TextFormField(
      obscureText: true,
      controller: _passwordController,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Password Required';
        }
        return null;
      },
      decoration: InputDecoration(
        label: Text('Password'),
        prefixIcon: Icon(LineIcons.alternateUnlock),
      ),
    );
  }

  Widget _footerLogin() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('No account?'),
        SizedBox(
          width: 10,
        ),
        InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => RegisterScreen()));
          },
          child: Text('Sign up', style: TextStyle(color: Colors.indigoAccent)),
        )
      ],
    );
  }

  Widget _buttonLogin() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.indigo,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 50)),
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              AuthDao().login(_emailController.text, _passwordController.text);
              Navigator.pushNamed(context, 'home');
            }
          },
          child: Text(
            'Login',
            style: TextStyle(fontSize: 20),
          )),
    );
  }
}
