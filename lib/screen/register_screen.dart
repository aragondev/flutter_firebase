// ignore_for_file: unnecessary_string_escapes

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase/data/auth_dao.dart';
import 'package:flutter_firebase/data/user.dart';
import 'package:flutter_firebase/data/user_dao.dart';
import 'package:flutter_firebase/screen/home_screen.dart';
import 'package:flutter_firebase/screen/login_screen.dart';
import 'package:flutter_firebase/widgets/appbar_widget.dart';
import 'package:line_icons/line_icons.dart';
import 'package:crypto/crypto.dart';
import 'dart:async';
import 'dart:io';

enum Gender { male, female }

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formGlobalKey = GlobalKey<FormState>();
  //controllers
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _genderController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  var _value = Gender.male;
  final _userR = UserRepository();
  final _auth = AuthDao();
  bool validateExistEmail = false;

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [_appBarRegister(), _formRegister(context)],
          ),
        ),
      ),
    );
  }

  Widget _appBarRegister() {
    return AppBarWidget(
      title: 'Register',
      size: 180,
      url: 'register.png',
    );
  }

  Widget _formRegister(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Form(
          key: _formGlobalKey,
          child: Column(
            children: [
              _nameInput(),
              _emailInput(),
              _genderInput(),
              _phoneInput(),
              _passwordInput(),
              _confirmPasswordInput(),
              _registerButton(),
              _footerRegister()
            ],
          )),
    );
  }

  bool isEmailValid(String email) {
    RegExp regex = RegExp('[^]*?@[^]*?\.[^]');
    return regex.hasMatch(email);
  }

  Widget _emailInput() {
    return TextFormField(
      controller: _emailController,
      validator: (value) {
        if (!isEmailValid(value!)) {
          return 'Enter a valid email address';
        }
      },
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          label: Text('Email'), prefixIcon: Icon(LineIcons.mailBulk)),
    );
  }

  Widget _nameInput() {
    return TextFormField(
      controller: _nameController,
      textCapitalization: TextCapitalization.sentences,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Please enter some text";
        }
      },
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          label: Text('Full Name'), prefixIcon: Icon(LineIcons.user)),
    );
  }

  Widget _genderInput() {
    return TextFormField(
      onTap: () {        
        FocusScope.of(context).requestFocus(new FocusNode());

        showModalBottomSheet(
            context: context,
            builder: (context) {
              return Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('CHOOSE AN OPTION'),
                    RadioListTile(
                        title: Text('Male'),
                        secondary: Icon(Icons.male),
                        value: Gender.male,
                        toggleable: true,
                        groupValue: _value,
                        onChanged: (value) {
                          if (_value == Gender.female) {
                            setState(() {
                              _value = value as Gender;
                              _genderController.text = 'Male';
                            });
                          } else {
                            _genderController.text = 'Male';
                          }
                          Navigator.pop(context);
                        }),
                    RadioListTile(
                        title: Text('Female'),
                        secondary: Icon(Icons.female),
                        value: Gender.female,
                        toggleable: true,
                        groupValue: _value,
                        onChanged: (value) {
                          setState(() {
                            _value = value as Gender;
                            _genderController.text = 'Female';
                          });
                          Navigator.pop(context);
                        }),
                  ],
                ),
              );
            });
      },
      controller: _genderController,
      decoration: InputDecoration(
          label: Text('Gender'), prefixIcon: Icon(LineIcons.male)),
    );
  }

  Widget _phoneInput() {
    return TextFormField(
      controller: _phoneNumberController,
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value!.isNotEmpty) {
          if (value.length < 8) {
            return 'Your telephone number is short';
          }
        } else {
          return 'Please add a number';
        }
      },
      decoration: InputDecoration(
          label: Text('Phone Number'), prefixIcon: Icon(LineIcons.phone)),
    );
  }

  Widget _passwordInput() {
    return TextFormField(
      obscureText: true,
      validator: (value) {
        if (value!.isNotEmpty) {
          if (value.length <= 5) {
            return 'Your password must have more than 7 characters';
          }
        } else {
          return 'Please add a password';
        }
      },
      decoration: InputDecoration(
          label: Text('Password'), prefixIcon: Icon(LineIcons.alternateUnlock)),
    );
  }

  Widget _confirmPasswordInput() {
    return TextFormField(
      controller: _confirmPasswordController,
      obscureText: true,
      validator: (value) {
        if (value!.isNotEmpty) {
          if (value == _passwordController.text) {
            return 'Your password and confirm password are not equals';
          }
        } else {
          return 'Please add a password';
        }
      },
      decoration: InputDecoration(
          label: Text('Confirm Password'),
          prefixIcon: Icon(LineIcons.alternateUnlock)),
    );
  }

  Widget _registerButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.indigo,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 50)),
          onPressed: () async {
            if (_formGlobalKey.currentState!.validate()) {
              var bytes1 = utf8.encode(_passwordController.text);
              var digest1 = sha256.convert(bytes1);
              String checkEmail = "";
                  checkEmail = await _auth.signup(_emailController.text, digest1.toString());
              if (checkEmail=="email-already-in-use") {
               final snackBar = SnackBar(content: Text('The account already exists for that email'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }else{
                 _userR.addUser(User(
                    name: _nameController.text,
                    email: _emailController.text,
                    gender: _genderController.text,
                    phone: _phoneNumberController.text,
                    password: digest1.toString()));
                Navigator.pushReplacementNamed(
                    context, 'home',arguments: _auth.getUser());


                
              }
            }
          },
          child: Text(
            'Register',
            style: TextStyle(fontSize: 20),
          )),
    );
  }

  Widget _footerRegister() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('You have an account?'),
        SizedBox(width: 5,),
        InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => LoginScreen()));
          },
          child: Text('Sign In', style: TextStyle(color: Colors.indigoAccent)),
        )
      ],
    );
  }
}
