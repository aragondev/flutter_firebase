import 'package:flutter/material.dart';

class AppBarWidget extends StatefulWidget {
  final String title;  
  final double size;
  final String url;
  const AppBarWidget({ Key? key, required this.title,  this.size=400.0, required this.url }) : super(key: key);

  @override
  State<AppBarWidget> createState() => _AppBarWidgetState();
}

class _AppBarWidgetState extends State<AppBarWidget> {  
  TextStyle textStyle = TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.black87);
  @override
  Widget build(BuildContext context) {    
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image(image:  AssetImage('assets/${widget.url}'),width: double.infinity,height: widget.size),
        Padding(
          padding: const EdgeInsets.only(left: 30),
          child: Text(widget.title,style: textStyle,),
        )
      ],
    );
  }
}