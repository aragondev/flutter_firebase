import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';

class AuthDao extends ChangeNotifier {
  final auth = FirebaseAuth.instance;

  bool isLoggedIn() {
    return auth.currentUser != null;
  }



  String? userId() {
    return auth.currentUser?.uid;
  }

   String? email() {
    return auth.currentUser!.email;
  }

  User? getUser() {
    return  auth.currentUser;
  }

  Future<String> signup(String email, String password) async {
     
    try {
      await auth.createUserWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseAuthException catch (e) {
      return e.code;      
    }  
    
    return "OK";   
  }

  Future<bool> login(String email, String password) async {
    String? errorMessage;
    try {
      await auth.signInWithEmailAndPassword(email: email, password: password);
      notifyListeners();
    } on FirebaseAuthException catch (e) {
      errorMessage = e.code;
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    }
    if (errorMessage != null) {
      return Future.error(errorMessage);
    }

    return true;
  }

  Future<void> logout() async {
    Future.wait([auth.signOut()]);
  }
}
