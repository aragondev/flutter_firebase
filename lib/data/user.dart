import 'package:cloud_firestore/cloud_firestore.dart';

// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

User userFromJson(Map<String, dynamic> json) {
  return User(
      name:  json['name'] as String,
      email: json['email'] as String,
      gender: json['gender'] as String,
      phone: json['phone'] as String,
      password: json['password'] as String
      );
}
  Map<String, dynamic> userToJson(User instance) => <String, dynamic>{
        'name': instance.name,
        'email': instance.email,
        'gender': instance.gender,
        'phone': instance.phone,
        'password': instance.password
      };


class User {
  User({
    required this.name,
    required this.email,
    required this.gender,
    required this.phone,
    required this.password,
    this.referenceId
  });

  final String name;
  final String email;
  final String gender;
  final String phone;
  final String password;
  String? referenceId;

  factory User.fromJson(Map<String, dynamic> json) => User(
        name: json["name"],
        email: json["email"],
        gender: json["gender"],
        phone: json["phone"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "gender": gender,
        "phone": phone,
        "password": password,
      };

  factory User.fromSnapshot(DocumentSnapshot snapshot) {
    final user = User.fromJson(snapshot.data() as Map<String, dynamic>);
    user.referenceId = snapshot.reference.id;
    return user;
  }
}
