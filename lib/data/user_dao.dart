import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_firebase/data/auth_dao.dart';

import 'user.dart';

class UserRepository {
  // 1
  final CollectionReference users =
      FirebaseFirestore.instance.collection('users');
  // 2
  Stream<QuerySnapshot> getStream() {
    return users.snapshots();
  }
  // 3
  Future<DocumentReference> addUser(User user) {
    return users.add(user.toJson());
  }

  
  // 4
  void updateUser(User user) async {
    await users.doc(user.referenceId).update(user.toJson());
  }
  // 5
  void deleteUser(User user) async {
    await users.doc(user.referenceId).delete();
  }
}
